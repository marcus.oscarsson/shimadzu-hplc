import urllib.request
import urllib.parse
import urllib.error
import urllib.request
import urllib.error
import urllib.parse
import re
from lxml import etree

cbm20_url = "http://160.103.149.143"


class REQUEST_DATA:
    LOGIN = """
        <Login>
        <Mode>0</Mode>
        <Certification>
        <UserID>{0}</UserID>
        <Password>{1}</Password>
        <SessionID/>
        <Result/>
        </Certification>
        </Login>"""

    LOGOUT = "<Login><Mode>-1</Mode></Login>"

    SET_PUMP_ON = "<Event><Method><PumpBT>{0}</PumpBT></Method></Event>"

    SET_PUMP_VOLUME_PARAMETERS = """
        <Method>
        <No>0</No>
        <Pumps>
        <Pump>
        <UnitID>A</UnitID>
        <Usual>
        <Flow>{0}</Flow>
        <Tflow>{1}</Tflow>
        </Usual>
        </Pump>
        </Pumps>
        </Method>"""

    SET_PUMP_PRESURE_PARAMETERS = """"
        <Method>
        <No>0</No>
        <Pumps><Pump>
        <UnitID>A</UnitID>
        <Usual>
        <Pmax>{0}</Pmax>
        </Usual>
        <Detail>
        <Pmin>{1}</Pmin>
        </Detail>
        </Pump></Pumps></Method>"""

    SET_FLOW_MODE = "<Config><SelMode>{0}</SelMode></Config>"

    SET_VALVE = """<?xml version="1.0"?>
        <Method>
        <No>0</No>
        <Alias>Method00</Alias>
        <Pumps>
        <Pump>
        <UnitID>A</UnitID>
        <Detail>
        <Psv>{0}</Psv>
        </Detail>
        </Pump>
        </Pumps>
        </Method>
        """

    SETUP_PURGE = """<?xml version="1.0"?><SetUp><Purge/></SetUp>"""

    START_AUTO_PURGE = """<?xml version="1.0"?>
        <SetUp>
        <Purge>
        <PurgeCheck>
        <ModuleSt1>{0}</ModuleSt1>
        <ModuleSt2>{1}</ModuleSt2>
        </PurgeCheck>
        <PurgeSetting>
        <SelModule1>100</SelModule1>
        <SelModule2>100</SelModule2>
        <ModuleT1>50.0</ModuleT1>
        <ModuleT2>50.0</ModuleT2>
        </PurgeSetting>
        <PurgeOther>
        <InitSt>0</InitSt>
        <InitT>10</InitT>
        <WarmSt>0</WarmSt>
        <WarmT>10</WarmT>
        </PurgeOther>
        <PurgeEvent><SelModuleNo>0</SelModuleNo><PurgeAct>1</PurgeAct></PurgeEvent></Purge>
        </SetUp>
        """

    STOP_AUTO_PURGE = """<?xml version="1.0"?>
        <SetUp>
        <Purge>
        <PurgeEvent><SelModuleNo>0</SelModuleNo><PurgeAct>0</PurgeAct></PurgeEvent></Purge>
        </SetUp>
        """

    SET_INJECT_PARAMETERS = """
        <Sequence><No>0</No><EditType>0</EditType><TotalSeqRowNum/><SeqRows><SeqRow>
        <RowNo>1</RowNo>
        <Rack>1</Rack>
        <Vial>
        <From>{0}</From>
        <To>{0}</To>
        </Vial>
        <InjNum>1</InjNum>
        <InjVol>{1}</InjVol>
        <SelMethodNo>255</SelMethodNo>
        <SelMethodName>-</SelMethodName>
        <StopTime>{2}</StopTime>
        </SeqRow></SeqRows><TotalInjNum/></Sequence>
        """

    # SET_INJECT_PARAMETERS = """<?xml version="1.0"?>
    #     <Sequence><No>0</No>
    #     <DispStart>1</DispStart>
    #     <DispEnd>10</DispEnd>
    #     <EditStart>1</EditStart>
    #     <EditEnd>1</EditEnd>
    #     <EditType>1</EditType><TotalSeqRowNum></TotalSeqRowNum>
    #     <SeqRows><SeqRow>
    #     <RowNo>1</RowNo>
    #     <Rack>1</Rack>
    #     <Vial>
    #     <From>{0}</From>
    #     <To>{0}</To>
    #     </Vial>
    #     <InjNum>1</InjNum>
    #     <InjVol>{1}</InjVol>
    #     <SelMethodNo>0</SelMethodNo>
    #     <SelMethodName>Method00</SelMethodName>
    #     <StopTime>{2}</StopTime>
    #     </SeqRow>
    #     </SeqRows><TotalInjNum/></Sequence>"""

    START_SEQUENCE = """<?xml version="1.0"?>
        <Event><Sequence><RunBT>1</RunBT></Sequence></Event>"""

    STOP_SEQUENCE = """<?xml version="1.0"?>
        <Event><Sequence><RunBT>0</RunBT></Sequence></Event>"""

    SET_WAVELENGTH_PARAMETERS = """<Method>
        <No>0</No>
        <PDA>
        <Usual>
        <Wave1>{0}</Wave1>
        <Wave2>{1}</Wave2>
        <Wave3>{2}</Wave3>
        <Wave4>{3}</Wave4>
        </Usual>
        </PDA>
        </Method>"""

    GET_MONITOR = """<?xml version="1.0"?><Monitor/>"""

    SET_MIX_PARAMETERS = """<?xml version="1.0"?><Method>
        <No>0</No>
        <Alias>Method00</Alias>
        <Pumps>
        <Pump>
        <UnitID>A</UnitID>
        <Usual>
        <Bconc>{0}</Bconc>
        <Cconc>{1}</Cconc>
        <Dconc>{2}</Dconc>
        </Usual>
        </Pump>
        </Pumps>
        </Method>"""

    GET_METHOD = """<?xml version="1.0"?><Method/>"""

    GET_CONFIG = "<Config/>"

    SET_AUTO_SAMPLER_TEMP = """<Method>
        <No>0</No>
        <AutoSamp>
        <Usual>
        <CoolTemp>{0}</CoolTemp>
        </Usual>
        </AutoSamp>
        </Method>"""

class ShimadzuCBM20:
    def __init__(self, url, debug=False):
        self.debug = debug
        self._url = url
        self._username = None
        self._session_id = None

    def _cbm20_query(self, cgi, data, path="/cgi-bin/"):
        req = urllib.request.Request(self._url + path + cgi, data.encode())
        req.add_header("Content-Type", "text")
        response = urllib.request.urlopen(req)
        _html = response.read()

        if self.debug:
            print(data)
            print(_html)
            
        html = etree.fromstring(_html)
        return html

    def login(self, username, password):
        # Only one users is allowed at a time, and sessions sometimes linger
        # so logout current user before logging in again
        self.logout()

        session_id = (
            self._cbm20_query(
                "Login.cgi", REQUEST_DATA.LOGIN.format(username, password)
            )
            .xpath("//SessionID")[0]
            .text
        )

        if not session_id:
            raise ConnectionError("Could not login")

        self._session_id = session_id
        self._username = username
        return str(session_id)

    def logout(self):
        return self._cbm20_query("Login.cgi", REQUEST_DATA.LOGOUT)

    def _switch_pump(self, on):
        return self._cbm20_query(
            "Event.cgi", REQUEST_DATA.SET_PUMP_ON.format(1 if on else 0)
        )

    def set_pump_volume_paramters(self, volume, rate):
        self._cbm20_query(
            "Method.cgi", REQUEST_DATA.SET_PUMP_VOLUME_PARAMETERS.format(float(rate), float(volume))
        )

    def set_pump_presure_parameters(self, pmin, pmax):
        self._cbm20_query(
            "Method.cgi", REQUEST_DATA.SET_PUMP_PRESURE_PARAMETERS.format(float(pmax), float(pmin))
        )

    def start_pump(self):
        return self._switch_pump(True)

    def stop_pump(self):
        return self._switch_pump(False)

    def get_config(self):
        return self._cbm20_query("Config.cgi", REQUEST_DATA.GET_CONFIG)

    def select_flow_mode(self, mode):
        # mode can be 'isocratic' or 'binary'(LabSolution)==LPGE
        if mode == "isocratic":
            value = 0
        else:
            value = 3  # LPGE
        return self._cbm20_query("Config.cgi", REQUEST_DATA.SET_FLOW_MODE.format(value))

    def select_solenoid_valve(self, valve_number):
        return self._cbm20_query(
            "Method.cgi", REQUEST_DATA.SET_VALVE.format(valve_number)
        )

    def get_flow_mode(self):
        cfg = self.get_config()
        selmode = int(cfg.xpath("//selmode")[0].text)

        if selmode == 0:
            # isocratic
            return "isocratic"
        elif selmode == 3:
            return "LPGE"
        else:
            return "other"

    # before starting purge, one has to be select pump mode
    # and valve position/port/'buffer bottle'
    def start_autopurge(self):
        # in isocratic mode, only module 1 has to be purged
        # in LPGE mode (binary), modules 1 and 2 have to be purged
        flow_mode = self.get_flow_mode()

        if flow_mode == "isocratic":
            self._cbm20_query(
                "Setup.cgi", REQUEST_DATA.SETUP_PURGE
            )
            return self._cbm20_query(
                "Setup.cgi", REQUEST_DATA.START_AUTO_PURGE.format(1, 0)
            )
        elif flow_mode == "LPGE":
            self._cbm20_query(
                "Setup.cgi", REQUEST_DATA.SETUP_PURGE
            )
            return self._cbm20_query(
                "Setup.cgi", REQUEST_DATA.START_AUTO_PURGE.format(1, 1)
            )
        else:
            raise RuntimeError("Invalid pump mode: %d." % flow_mode)

    def stop_autopurge(self):
        self._cbm20_query("Setup.cgi", REQUEST_DATA.SETUP_PURGE)
        return self._cbm20_query("Setup.cgi", REQUEST_DATA.STOP_AUTO_PURGE)

    def inject_from_vial(self, vial, vol, runtime=1):
        """Inject vol uL from specified vial"""
        self._cbm20_query(
            "Seq.cgi", REQUEST_DATA.SET_INJECT_PARAMETERS.format(vial, vol, runtime)
        )
        
        return self._cbm20_query("Event.cgi", REQUEST_DATA.START_SEQUENCE)

    def stop_inject(self):
        return self._cbm20_query("Event.cgi", REQUEST_DATA.STOP_SEQUENCE)

    def set_wavelengths(self, w1, w2, w3, w4):
        return self._cbm20_query(
            "Method.cgi", REQUEST_DATA.SET_WAVELENGTH_PARAMETERS.format(w1, w2, w3, w4)
        )

    def set_auto_sampler_temp(self, temp):
        return self._cbm20_query(
            "Method.cgi", REQUEST_DATA.SET_AUTO_SAMPLER_TEMP.format(float(temp))
        )

    def get_monitor(self):
        return self._cbm20_query("Monitor.cgi", REQUEST_DATA.GET_MONITOR)

    def get_error(self):
        monitor = self.get_monitor()
        error = monitor.xpath("//errormon")[0]
        return {
            "type": str(error.xpath("//errortype")[0].text),
            "code": str(error.xpath("//errorcode")[0].text),
            "extra": [
                str(x.text).split() for x in error.findAll(re.compile("errorextinfo+"))
            ],
            "unit": str(error.xpath("//errorunit")[0].text),
        }

    def mix_from_port(self, a, b, c, d):
        if (a + b + c + d) != 100:
            raise ValueError("Percentage of components exceeds 100%.")
        return self._cbm20_query(
            "Method.cgi", REQUEST_DATA.SET_MIX_PARAMETERS.format(b, c, d)
        )

    def pump_from_port(self, port):
        result = ""

        if port == "a":
            result = self.mix_from_port(100, 0, 0, 0)
        elif port == "b":
            result = self.mix_from_port(0, 100, 0, 0)
        elif port == "c":
            result = self.mix_from_port(0, 0, 100, 0)
        elif port == "d":
            result = self.mix_from_port(0, 0, 0, 100)

        return result

    def get_method(self):
        return self._cbm20_query("Method.cgi", REQUEST_DATA.GET_METHOD)


cbm20 = ShimadzuCBM20(cbm20_url, debug=True)
cbm20.login("Admin", "Admin")

import pdb
pdb.set_trace()

cbm20.set_auto_sampler_temp(25)
cbm20.set_pump_volume_paramters(1.1, 1.1)
cbm20.set_pump_presure_parameters(0.1, 1)
cbm20.set_wavelengths(280, 215, 260, 234)
cbm20.pump_from_port('a')
cbm20.start_pump()

cbm20.inject_from_vial(1, 10, 1)
cbm20.logout()

#cbm20.start_pump()

cbm20.stop_pump()


import pdb
pdb.set_trace()
