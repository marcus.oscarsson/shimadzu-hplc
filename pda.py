import urllib.request
import urllib.parse
import urllib.error
import urllib.request
import urllib.error
import urllib.parse
import re
from lxml import html

cbm20_url = "http://160.103.149.143"
spectro_url = "http://160.103.149.142"


class PDA:
    def __init__(self, url, debug=False):
        self.debug = debug
        self._url = url
        self._username = None
        self._password = None

    def login(self, username, password):
        self._username = username
        self._password = password

        login_data = """<?xml version="1.0"?>
        TXT_MEMO={0}&BTN_PASSW={0}&BTN_LOGIN=++++Login+++""".format(
            username, password
        )

        req = urllib.request.Request(spectro_url + "/cgi-bin/logina", login_data)
        urllib.request.urlopen(req)

    def logout(self):
        req = urllib.request.Request(spectro_url + "/cgi-bin/logout")
        urllib.request.urlopen(req)

    def read(self):
        req = urllib.request.Request(spectro_url + "/cgi-bin/pallg")
        response = urllib.request.urlopen(req)
        _html = response.read()
        data = [
            x.strip()
            for x in html.fromstring(_html).xpath("//td//text()")
            if x.strip() not in ["\n", ""]
        ]

        result = []
        for i in range(0, 4):
            values = data[9 + (i * 9) : 18 + (i * 9)]
            result.append(
                {
                    "channel": values[0],
                    "wavelength": {"value": float(values[1]), "unit": values[2]},
                    "absorbance": {"value": float(values[3]), "unit": values[4]},
                    "bandwidth": {"value": float(values[5]), "unit": values[6]},
                    "range": values[7],
                    "polarity": values[8],
                }
            )
        return result


# cbm20 = ShimadzuCBM20(cbm20_url)
# cbm20.login("Admin", "Admin")

pda = PDA(spectro_url)

import time

while True:
    time.sleep(0.5)
    print(pda.read())

import pdb

pdb.set_trace()
# stop_pump()
# set_components(0, 0, 100, 0)
# start_pump()
